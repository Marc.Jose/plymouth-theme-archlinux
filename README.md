# Plymouth Archlinux Theme
*Plymouth-Archlinux* is a theme for [Plymouth](https://gitlab.freedesktop.org/plymouth/plymouth), one of the boot splash tools for Linux.
It's designed with [Archlinux](https://archlinux.org) in mind, but can be used in any installation that uses Plymouth.    

## Inspiration
The theme is inspired by the following Plymouth/Grub themes:   
* [Dark Arch](https://github.com/jsayol/plymouth-theme-dark-arch)      
* [Ubuntu Darwin](https://github.com/ashutoshgngwr/ubuntu-darwin)      
* [Calededonia Tux](https://store.kde.org/p/1009320/)      
* [Xenlism's Grub Themes](https://github.com/xenlism/Grub-themes)    

## How to use the theme  
Clone this repository to `/usr/share/plymouth/themes/`  and run `sudo plymouth-set-default-theme -R archlinux` .  
